#include <stdio.h>

#define SIZE_HEX 16
#define SIZE_BINARY 33
#define OFFSET 10

/*=====  converts hexadecimal numbers to decimal numbers    ==================*/
int hex_to_dec(char *input, int string_size)   	
{
	int i=0, multiplier=1, digit=-1000,  div_result=0, div_reminder=0;
	long decimal_no=0;
	/*strlen counts from 1 table index starts from 0 */
	for(i=string_size-1;i>=0;i--){
		input[i]= toupper(input[i]);
		if((input[i]-'0')<=9)
			digit = input[i]-'0';
		else if( ((input[i]-'A')>=0) && ((input[i]-'A')<=5) )
			digit = input[i]-'A'+OFFSET;
		else{
			printf("\ninvalid entry");
			return 0;
		}
		decimal_no = decimal_no + digit * multiplier;
		multiplier *= 16;
	}
	printf("\n\tdec:\t%d", decimal_no);
	return decimal_no;
}
/*=================converts binary numbers to decimal numbers ================*/
int bin_to_dec(char *input, int max_table_index)
{
	int i=0, decimal_no=0, input_digit=0;
	
	for(i=max_table_index;i>=0;i--){
		if(input[i]=='0')
			input_digit = 0;
		else if(input[i]=='1')
			input_digit = 1;
		decimal_no = decimal_no + input_digit*(1<<(max_table_index-i));
	}
	printf("\n\tdec:\t%d", decimal_no);
	return decimal_no;
}
/*============ converts decimal numbers to binary numbers  ===================*/
void dec_to_bin(long decimal_no)
{
	short tab_bin[SIZE_BINARY]={0};
	int i=0, max_i=0, div_result=0, tmp_decimal_no ;

	tab_bin[i]=decimal_no%2;
	do{
		tmp_decimal_no=decimal_no;
		i=0;
		while(tmp_decimal_no > 1){
			i++;
			tmp_decimal_no = tmp_decimal_no>>1;
		}
		if(i!=0)
			tab_bin[i]=1;
		if(i>max_i)
			max_i=i;
	
		decimal_no-=(1<<i);
	}while(i>0);
	printf("\n\tbinary:\t");
	for(max_i; max_i>=0; max_i-- )
		printf("%d", tab_bin[max_i]);
}
/*=============== converts decimal numbers to hexadecimal numbers ============*/
void dec_to_hex(long decimal_no)
{
	char tab_hex[SIZE_HEX] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		   				       'A', 'B', 'C', 'D', 'E', 'F'};
	char hexadecimal_digit[SIZE_HEX];
	int i=0, div_result=0, div_reminder=0;
	
	do{
		div_result = decimal_no/16;
		div_reminder = decimal_no%16;
		decimal_no = div_result;
		hexadecimal_digit[i] = tab_hex[div_reminder];
		i++;
		if(decimal_no < 16)
		    hexadecimal_digit[i] = tab_hex[decimal_no];
		
	}while(decimal_no >= 16);

	printf("\n\thex:\t0x");
	for(i; i>=0; i-- ){
	    printf("%c", hexadecimal_digit[i]);
	}	
}
/*============================================================================*/
