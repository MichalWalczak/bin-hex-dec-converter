/* This is what i have to say about my project:
 *
 * "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
 * sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
 * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
 * nisi ut aliquip ex ea commodo consequat."
 * 
 */
#ifndef _DEC_HEX_BIN_H_
#define _DEC_HEX_BIN_H_
/*=======================================================+=======================
						methodes
========================================================+=======================*/

int hex_to_dec(char *, int);/* converts hexadecimal numbers to decimal numbers 	*/
int bin_to_dec(char *, int);/* converts binary numbers to decimal numbers 		*/
void dec_to_bin(long);  	/* converts decimal numbers to binary numbers 		*/
void dec_to_hex(long ); 	/* converts decimal numbers to hexadecimal numbers 	*/

#endif /*_DEC_HEX_BIN_H_ */
