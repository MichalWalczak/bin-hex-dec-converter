#include <stdio.h>
#include <string.h>
#include "dec_hex_bin.h"

#define SIZE_OF_STRING 16
#define HEX_TO_OTHERS 1
#define BIN_TO_OTHERS 2
#define DEC_TO_OTHERS 3
#define EXIT 4

void manu();
int main(int argc, char *argv[]) 
{
	char input[SIZE_OF_STRING];
	int max_table_index=0,i=0, read_success=0, menu_option=0;
	long decimal_numbeer=0;
	do{
		manu();
		read_success=scanf("%d",&menu_option);
		if (!read_success) { printf("\t invalid entry\n");
			fflush(stdin); continue; }

		switch (menu_option){
			case HEX_TO_OTHERS:{
				printf("\t1 - Enter hexadecimal number \n\t0x");
				if(scanf("%15s",&input)){
				decimal_numbeer = hex_to_dec(input, strlen(input));
				dec_to_bin(decimal_numbeer);
				}
			}break;
			case BIN_TO_OTHERS:{
				printf("\t2 - Enter binary number\n\t");
				if(scanf("%15s",&input)){
				decimal_numbeer = bin_to_dec(input, strlen(input));
				dec_to_hex(decimal_numbeer );
				}
			}break;
			case DEC_TO_OTHERS:{
				printf("\t3 - Enter decimal number\n\t");
				scanf("%d",&decimal_numbeer);
				dec_to_bin(decimal_numbeer);
				dec_to_hex(decimal_numbeer );
			}break;				
			case EXIT:{
				printf("\t4 - the end\n");			
			}break;
			default:break;
		}
	}while(menu_option!=4);
	return 0;
}
void manu()
{
	printf("\n\n\t\tMAIN MENU\n\n");	
	printf("\t1 - hex_to_others \n");
	printf("\t2 - bin_to_others\n");
	printf("\t3 - dec_to_others \n");
	printf("\t4 - end program\n\n\t");	
}
